workdir=.
syst=trk_eff
merged_hist_path=/eos/home-w/wasu/HEP_Repo/QG_Calibration/NewWorkflow/trained_lightGBM_new/${syst}

for reweighting in jet_nTracks_sys_eff jet_nTracks_sys_fake 
do
    echo 'doing plot for ${reweighting}...'
    python -u ${workdir}/core/Calculate_SF.py \
    --path-mc ${merged_hist_path}/${reweighting}/MC_merged_hist.pkl \
    --path-data ${merged_hist_path}/${reweighting}/Data_merged_hist.pkl \
    --period ADE --reweighting  quark\
    --output-path ${workdir}/results_CalculateSF/${syst}/${reweighting}/
done
