syst='JESJER'
workdir=/eos/home-w/wasu/NewWorkflow
merged_hist_path=/eos/home-w/wasu/HEP_Repo/QG_Calibration/NewWorkflow/trained_lightGBM_new/${syst}
cd ${merged_hist_path}
for file in ./* 
do
    echo 'doing plot for ${file}...'
    python -u ${workdir}/core/Calculate_SF.py \
    --path-mc ${merged_hist_path}/${file}/MC_merged_hist.pkl \
    --path-data ${merged_hist_path}/${file}/Data_merged_hist.pkl \
    --period ADE --reweighting quark\
    --output-path ${workdir}/results_CalculateSF/${syst}/${file}/
done
